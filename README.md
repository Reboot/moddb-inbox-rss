# 📡 ModDB Inbox RSS

Small Flask app that provides an RSS feed for your ModDB updates.

## Instructions

1. `git clone https://codeberg.org/KitsuhimeGrimoire/moddb-inbox-rss.git`
2. `cd moddb-inbox-rss`
3. `python3 -m venv venv`
4. `source venv/bin/activate`
5. `pip install -r requierements.txt`
6. `flask run`
