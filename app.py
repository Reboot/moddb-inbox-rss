from os.path import exists
from flask import Flask, make_response
import yaml
from classes.ModdbInboxRSS import ModdbInboxRSS

CONFIG_FILE = "config.yaml"
CONFIG_DEFAULTS = dict(
        username_path = "/run/secrets/username",
        password_path = "/run/secrets/password",
        cookies_file  = "./cookies.txt",
        save_cookies_to_file = False
)

config = dict()
username = ""
password = ""

app = Flask("moddb-inbox-rss")


def process_config():
    global config, username, password
    if exists(CONFIG_FILE):
        with open(CONFIG_FILE, "r") as stream:
            try:
                loaded_conf = yaml.safe_load(stream)
                for key in CONFIG_DEFAULTS:
                    if key in loaded_conf:
                        config[key] = loaded_conf[key]
                        print(f"Using config file value for {key}")
                    else:
                        config[key] = CONFIG_DEFAULTS[key]
                        print(f"No config file value for {key}, using defaults")
            except yaml.YAMLError:
                print("Invalid YAML, using defaults")
                config = CONFIG_DEFAULTS
    else:
        print("No config file found, using defaults")
        config = CONFIG_DEFAULTS

    with open(config["username_path"], "r") as stream:
        try:
            username = stream.read().strip()
        except yaml.YAMLError as exc:
            print(exc)

    with open(config["password_path"], "r") as stream:
        try:
            password = stream.read().strip()
        except yaml.YAMLError as exc:
            print(exc)


@app.route('/feed')
def feed():
    
    if config["save_cookies_to_file"]:
        moddb_inbox = ModdbInboxRSS(username, password, config["cookies_file"])
    else:
        moddb_inbox = ModdbInboxRSS(username, password, None)

    feed = moddb_inbox.get_feed()
    response = make_response(feed.rss())
    response.headers['Content-Type'] = 'application/rss+xml'
    return response


process_config()
