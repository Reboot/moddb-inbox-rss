from os.path import exists
from twill import browser
from twill.commands import *
import lxml.html as LH
from lxml import etree
from datetime import datetime
from rfeed import *


class Content(Extension):
    def get_namespace(self):
        return {"xmlns:content": "http://purl.org/rss/1.0/modules/content/"}


class ContentItem(Serializable):
    def __init__(self, content):
        Serializable.__init__(self)
        self.content = content

    def publish(self, handler):
        Serializable.publish(self, handler)
        self._write_element("content:encoded", self.content)


class ModdbInboxRSS:

    FEED_ITEM_TYPE_MATCH = {
        "/features/": "Feature",
        "/news/": "Article",
        "/images/": "Image",
        "/videos/": "Video",
        "/downloads/": "Download",
        "/addons/": "Addon",
        "/tutorials/": "Tutorial",
        "/giveaways/": "Giveaway",
        "/polls/": "Poll"
    }

    credentials = dict()
    inbox_items = dict()
    cookies_path = None

    def __init__(self, username, password, cookies_path):
        self.credentials["username"] = username
        self.credentials["password"] = password
        
        if cookies_path != None:
            self.cookies_path = cookies_path

    def get_feed(self):
        inbox = self.get_inbox_html()
        self.sync(inbox)
        feed = Feed(
            title = "ModDB updates",
            link = "https://www.moddb.com/messages/updates",
            description = "RSS Feed for your ModDB updates",
            language = "en-US",
            lastBuildDate = datetime.now(),
            items = self.inbox_items.values(),
            extensions = [Content()]
        )
        return feed

    def get_inbox_html(self):
        if self.cookies_path != None and exists(self.cookies_path):
            browser.load_cookies(self.cookies_path)

        browser.go("https://www.moddb.com/messages/updates")

        # login if we're not already logged in
        if "Login to ModDB" in browser.html:
            print("Logging in using credentials")
            formvalue("membersform", "membersusername", self.credentials["username"])
            formvalue("membersform", "memberspassword", self.credentials["password"])
            submit()
        else:
            print("Logged in using cookies")

        if "Login to ModDB" in browser.html:
            print("oops, failed to login...")
            return dict()

        if self.cookies_path != None:
            browser.save_cookies(self.cookies_path)
        
        return LH.fromstring(browser.html)

    def sync(self, inbox):

        stats = {'fetched': 0, 'removed': 0}
        new_feed_items = []
        print("Starting sync...")

        for section in inbox.xpath("//div[contains(@class, 'body bodysimple clear')]"):
            for row in section.xpath(".//div[contains(@class, 'row rowcontent clear')]"):
                for link in row.xpath("div/p/a"):
                    new_feed_items.append(link.get("href"))

        # Only fetch new items
        for new_link in new_feed_items:
            item_exists = False
            for link, feed_item in self.inbox_items.items():
                if new_link == link:
                    item_exists = True
                    break
            if not item_exists:
                self.inbox_items[new_link] = self.fetch_item(new_link)
                stats['fetched'] += 1

        # Remove feed items in dict but no longer in inbox
        to_delete = []
        for link, feed_item in self.inbox_items.items():
            item_exists = False
            for new_link in new_feed_items:
                if new_link == link:
                    item_exists = True
                    break
            if not item_exists:
                to_delete.append(link)
        # We can't just delete them in the previous loop because otherwise
        # Python complains about the size of the dict changing during an interation
        for link in to_delete:
            self.inbox_items.pop(link)
            stats['removed'] += 1

        print(f"Sync complete: {len(self.inbox_items)} items in total, {stats['fetched']} new fetches, {stats['removed']} removed")

    def fetch_item(self, link):

        print(f"Fetching content for {link}")
        browser.go(link)
        html = LH.fromstring(browser.html)

        # Get item type
        for key, value in self.FEED_ITEM_TYPE_MATCH.items():
            if key in link:
                item_type = value
                break
        if item_type == "":
            item_type = "Item"

        # Parse content from item page
        # TODO: Don't crash when no item_type...

        page_name = ""
        item_name = html.xpath(".//div[contains(@class, 'first')]//span[contains(@class, 'heading')]")[0].text_content()
        if item_type == "Giveaway":  # Giveaways don't have the usual header
            page_name = item_name
        else:
            if len(html.xpath(".//div[contains(@class, 'headercorner')]//h2")) == 1:
                page_name = html.xpath(".//div[contains(@class, 'headercorner')]//h2")[0].text_content()

        if item_type == "Article" or item_type == "Feature" or item_type == "Giveaway" or item_type == "Tutorial":
            article_content = etree.tostring(html.xpath(".//div[contains(@id, 'articlecontent')]")[0], pretty_print=True, method="xml")
            item_content = article_content.decode("utf-8")
            date_string = html.xpath(".//div[contains(@id, 'readarticle')]//time")[0].get("datetime")
            description = html.xpath(".//p[contains(@class, 'introductiontext')]")[0].text_content()

        elif item_type == "Image" or item_type == "Video":
            embed = html.xpath(".//input[contains(@id, 'embedcode')]")[0].get("value")
            description = html.xpath(".//div[contains(@id, 'mediaabout')]//p")
            if len(description) > 0:  # Some media items have no descriptions
                description = description[0].text_content()
                item_content = f"{embed}<p>{description}</p>"
            else:
                description = ""
                item_content = embed
            date_string = html.xpath(".//div[contains(@id, 'mediaprofilemenu')]//time")[0].get("datetime")

        elif item_type == "Download" or item_type == "Addon":
            item_content = html.xpath(".//input[contains(@id, 'downloadwidget')]")[0].get("value")
            date_string = html.xpath(".//div[contains(@id, 'downloadsinfo')]//time")[0].get("datetime")
            description = html.xpath(".//p[contains(@id, 'downloadsummary')]")[0].text_content()

        # TODO: better poll support, parse the options and stuff...
        elif item_type == "Poll":
            item_content = html.xpath(".//p[contains(@class, 'question')]")[0].text_content
            #date_string = html.xpath(".//div[contains(@id, 'downloadsinfo')]//time")[0].get("datetime")
            description = html.xpath(".//p[contains(@class, 'question')]")[0].text_content

        else:
            print(f"{item_type} content type isn't handled")
            item_content = ""

        if ('date_string' in locals()):
            try:
                item_date = datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S+00:00")
            except:
                item_date = datetime.strptime(date_string, "%Y-%m-%d")
        else:
            item_date = None

        if page_name != "":
            title = f"{item_name} - {item_type} for {page_name}"
        else:
            title = f"{item_name} - {item_type}"

        print(title)

        item = Item(
            title = title,
            link = "https://www.moddb.com" + link,
            description = description,
            guid = Guid(link),
            author = page_name,
            pubDate = item_date,
            extensions = [ContentItem(item_content)]
        )

        return item
